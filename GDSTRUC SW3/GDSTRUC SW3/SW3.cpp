#include <iostream>
#include <string>

using namespace std;


void insertMembers(string* guildMembers, int memberCount)
{
	for (int i = 0; i < memberCount; i++)
	{
		cout << "Member:  "; cin >> guildMembers[i];
	}
}

void printMembers(string* guildMembers, int memberCount)
{
	for (int i = 0; i < memberCount; i++)
	{
		cout << "Member " << i + 1 << ": " << guildMembers[i] << endl;
	}
}

void renameMember(string* guildMembers, int memberPlace)
{
	string newName;

	cout << "New Name of the Member: "; cin >> newName;
	guildMembers[memberPlace - 1] = newName;
}

void addMember(string* guildMembers, string *tempArray, int memberCount, int additionalMem)
{
	for (int i = 0; i < (memberCount - additionalMem); i++)
	{
		guildMembers[i] = tempArray[i];
	}
	for (int i = 0; i < additionalMem; i++)
	{
		int x = 1;
		string newMember;
		cin >> newMember;
		guildMembers[memberCount - additionalMem] = newMember;
	}
}

void getCurrentArray(string* guildMembers, string *tempArray, int memberCount)
{
	for (int i = 0; i < memberCount; i++)
	{
		tempArray[i] = guildMembers[i];
	}
}
void deleteMember(string* &guildMembers, string *tempArray, int memberCount, int memberPlace)
{
	for (int i = 0; i < memberCount; i++)
	{
		if (guildMembers[i] == guildMembers[memberPlace - 1])
		{
			for (int k = i; k < memberCount - 1; k++)
			{
				guildMembers[k] == guildMembers[k + 1];
			}
		}
	}
		
}

int main()
{
	string guildName;
	int memberCount;
	int memberPlace;
	int choice;
	int additionalNum = 1;

	cout << "Guild Name: "; cin >> guildName;
	cout << "How many Members in your Guild? "; cin >> memberCount;

	string *guildMembers = new string[memberCount];
	insertMembers(guildMembers, memberCount);

	system("CLS");

	cout << guildName << " Members " << endl;
	printMembers(guildMembers, memberCount);

	while (true)
	{
		cout << "What do you want to Do?" << endl << endl;

		cout << "Print Members [1]" << endl;
		cout << "Rename a Member [2]" << endl;
		cout << "Add a Member [3]" << endl;
		cout << "Delete a Member [4]" << endl;
		cin >> choice;

		system("CLS");

		if (choice == 1)
		{
			printMembers(guildMembers, memberCount);
			system("pause");
		}

		else if (choice == 2)
		{
			cout << "From 1-" << memberCount << endl;
			cout << "What member do you want to rename? "; cin >> memberPlace;
			renameMember(guildMembers, memberPlace);
		}

		else if (choice == 3)
		{
			string *tempArray = new string[memberCount];
			getCurrentArray(guildMembers, tempArray, memberCount);

			memberCount += additionalNum;
			delete[] guildMembers;

			guildMembers = new string[memberCount];
			addMember(guildMembers, tempArray, memberCount, additionalNum);

			delete[] tempArray;
		}
		else if (choice == 4)
		{
			cout << "From 1-" << memberCount << endl;
			cout << "What member do you want to delete? "; cin >> memberPlace;
			string *tempArray = new string[memberCount];

			deleteMember(guildMembers, tempArray, memberCount, memberPlace);
			getCurrentArray(guildMembers, tempArray, memberCount);

			memberCount -= 1;

			delete[]guildMembers;
			guildMembers = new string[memberCount];

			for (int i = 0; i < (memberCount); i++)
			{
				guildMembers[i] = tempArray[i];
			}

			delete[]tempArray;

		}
		system("CLS");
	}
	system("pause");
	return 0;
}