#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void insertArray(int arrNum[])
{
	for (int i = 0; i < 10; i++)
	{
		arrNum[i] = rand() % 68 + 1;
	}
}
void arrayAscend(int arrNum[])
{
	int temp;
	for (int i = 0; i < 10; i++)
	{
		for (int k = 0; k < 10; k++)
		{
			if (arrNum[k] > arrNum[i])
			{
				temp = arrNum[i];
				arrNum[i] = arrNum[k];
				arrNum[k] = temp;
			}
		}
	}
}
void arrayDescen(int arrNum[])
{
	int temp;
	for (int i = 0; i < 10; i++)
	{
		for (int k = 0; k < 10; k++)
		{
			if (arrNum[k] < arrNum[i])
			{
				temp = arrNum[i];
				arrNum[i] = arrNum[k];
				arrNum[k] = temp;
			}
		}
	}
}
void printArray(int arrNum[])
{
	for (int i = 0; i < 10; i++)
	{
		cout << arrNum[i] << endl;
	}
}
void linearSearch(int arrNum[], int numSearch, int steps)
{
	for (int i = 0; i < 10; i++)
	{
		if (arrNum[i] == numSearch)
		{
			cout << numSearch << "is Found." << endl;
			cout << steps << "times it takes to do." << endl;
			break;
		}
		steps++;
	}
	steps++;
	if (steps == 11)
	{
		cout << numSearch << "is not Found." << endl;
		cout << steps - 1 << "times it takes to do." << endl;
	}
}

int main()
{
	srand(time(NULL));

	int arrNum[10];
	int numSearch;
	int steps = 0;
	string arrange;

	insertArray(arrNum);

	cout << "ASCENDING[A] or DESCENDING[D]" << endl;
	cin >> arrange;

	if (arrange == "A")
	{
		arrayAscend(arrNum);
	}

	else
	{
		arrayDescen(arrNum);	
	}

	printArray(arrNum);
	while (true)
	{
		cout << "[From 1 - 69]" << endl;
		cout << "Number to Search: "; cin >> numSearch;

		linearSearch(arrNum, numSearch, steps);
	}
	
	system("pause");
	return 0;
}