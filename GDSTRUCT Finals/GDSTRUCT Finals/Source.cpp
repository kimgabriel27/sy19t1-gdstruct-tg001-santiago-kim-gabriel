#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"


using namespace std;

int main()
{
	int size;
	int choice;
	int numberToAdd;
	int arraySize;
	cout << "Enter the size for element size: ";
	cin >> size;


	Stack<int>stackArray(size);
	Queue<int>queueArray(size);

	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "1  -  Push elements" << endl;
		cout << "2  -  Pop elements" << endl;
		cout << "3  -  Print everything then empty set" << endl;
		
		cout << "Choice: ";
		cin >> choice;


		if (choice == 1)
		{
			cout << "Enter number: ";
			cin >> numberToAdd;

			stackArray.push(numberToAdd);
			queueArray.push(numberToAdd);

			cout << "Top elements of sets:" << endl;
			cout << "Queue: " << queueArray[0] << endl;
			cout << "Stack: " << stackArray[0] << endl;
				
		}
		else if (choice == 2)
		{
			
			cout << "Top element has been pop" << endl;

			queueArray.pop();
			stackArray.pop();


			cout << "Top elements of sets:" << endl;
			cout << "Queue: " << queueArray[0] << endl;
			cout << "Stack: " << stackArray[0] << endl;

		}
		else
		{
			cout << "Queue Elements" << endl;
			for (int j = 0; j < queueArray.getSize(); j++)
			{
				cout << queueArray[j] << endl;
			}

			cout << "Stack Elements" << endl;
			for (int i = 0; i < stackArray.getSize(); i++)
			{
				cout << stackArray[i] << endl;
			}

			queueArray.remove();
			stackArray.remove();


		}

		system("pause");
		system("CLS");
	}
	
}