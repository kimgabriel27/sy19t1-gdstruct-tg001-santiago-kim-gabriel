#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"

using namespace std;



void main()
{
	srand(time(NULL));

	int size;
	int numberDelete;
	int searchNum;
	int searchIndex;

	cout << "How many intergers do you want?: "; cin >> size;

	UnorderedArray<int>values(size);

	for (int i = 0; i < size; i++)
	{
		int randValue = rand() % 100 + 1;
		values.push(randValue);
	}

	for (int i = 0; i < values.getSize(); i++)
	{
		cout << "Element " << i + 1 << ": " << values[i] << endl;
	}

	cout << "From 1 to " << values.getSize() << " What element do you want to remove? "; cin >> numberDelete;

	values.remove(numberDelete - 1);

	for (int i = 0; i < values.getSize(); i++)
	{
		cout << "Element " << i + 1 << ": " << values[i] << endl;
	}

	cout << "What number do you want to search? "; cin >> searchNum;
	
	values.linearSearch(searchNum, values);

	system("pause");
}