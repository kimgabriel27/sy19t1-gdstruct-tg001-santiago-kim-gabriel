#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

void main()
{
	srand(time(NULL));

	int size;
	int numberDelete;
	int searchNum;

	cout << "How many integers do you want?: "; cin >> size;

	UnorderedArray<int>unordered(size);
	OrderedArray<int>ordered(size);

	for (int i = 0; i < size; i++)
	{
		int randValue = rand() % 101;
		unordered.push(randValue);
		ordered.push(randValue);
	}

	system("CLS");

	while (true)
	{

		int chooseAction;

		cout << "Unordered Array: ";
		for (int i = 0; i < unordered.getSize(); i++)
		{
			cout << unordered[i] << " ";
		}

		cout << "\nOrdered Array: ";

		for (int i = 0; i < ordered.getSize(); i++)
		{
			cout << ordered[i] << " ";
		}


		cout << "\n\nWhat do you want to do: " << endl;
		cout << " 1 - Remove element at index" << endl;
		cout << " 2 - Search for an Element" << endl;
		cout << " 3 - Expand and generate random value/s" << endl;
		cin >> chooseAction;
		if (chooseAction == 1)
		{

			cout << "\n\nChoose index to delete: "; cin >> numberDelete;
			unordered.remove(numberDelete);
			ordered.remove(numberDelete);

			system("CLS");
		}
		else if (chooseAction == 2)
		{
		

			cout << "\n\nWhat number do you want to search? "; cin >> searchNum;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(searchNum);
			if (result >= 0)
				cout << searchNum << " was found at index " << result << ".\n";
			else
				cout << searchNum << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(searchNum, size);
			if (result >= 0)
				cout << searchNum << " was found at index " << result << ".\n";
			else
				cout << searchNum << " not found." << endl;

			system("pause");
			system("CLS");
		}
		else if (chooseAction == 3)
		{
			cout << "\n\nHow many elements do you want to Add? "; cin >> size;
			for (int i = 0; i < size; i++)
			{
				int randValue = rand() % 101;
				unordered.push(randValue);
				ordered.push(randValue);
			}
			system("CLS");
		}
	}
	system("pause");

}