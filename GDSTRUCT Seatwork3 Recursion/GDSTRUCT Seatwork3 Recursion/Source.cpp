#include <iostream>
#include <string>

using namespace std;

int sumDigits(int input, int sum)
{
	if (input <= 0)
		return sum;

	sum += input % 10;

	sumDigits(input / 10, sum);

}

void fibonacciSeq(int &f1, int &f2, int n)
{
	int seq;
	if (n <= 0)
		return;

	seq = f1 + f2;
	f1 = f2;
	f2 = seq;

	cout << seq << " ";

	fibonacciSeq(f1, f2, n - 1);

}

int checkPrime(int number, int i)
{
	
	if (number <= 2)
		return 1;
	if (number % i == 0)
		return 0;
	if (i * i > number)
		return 1;

	checkPrime(number, i + 1);

}

void main()
{
	int number;
	int sum = 0;
	int n, i = 2 ;
	int f1 = 0;
	int f2 = 1;
	int ifPrime = 1;

	cout << "Enter a number: ";
	cin >> number;

	cout << sumDigits(number, sum);

	system("pause");

	cout << "Enter n of Fibonacci Seq: ";
	cin >> n;

	cout << "0 1 ";
	fibonacciSeq(f1, f2, n - 2);

	system("pause");

	cout << "Check if Prime Num or Not." << endl;
	cout << " Enter a positive Integer: ";
	cin >> number;
	ifPrime = checkPrime(number, i);

	if (ifPrime == 1)
		cout << number << " is a Prime Number." << endl;
	else
		cout << number << " is not a Prime Number." << endl;
	system("pause");
}